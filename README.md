# Space Forge Prime

A Flask web application written as a solution to a challenge set by Space Forge. 

It is a web server that supports SSL, contained in a Docker image. 

It allows you to check for primes up to a certain number, and deliveres as output a list of primes and the code runtime as a JSON string. 

## Building
To build the Docker image, clone the repo using:
```
git clone https://gitlab.com/benclark101/space-forge-primes.git space_forge_primes
```

Make sure that you have Docker installed before proceeding, for example, on Ubuntu 20.04:
```
sudo apt update
sudo apt install docker.io
```

You can then compile the docker image by doing:
```
cd space_forge_primes
./docker/gen_docker.sh
```

## Running
Once the docker image is compiled, you can run it using:
```
./docker/docker_run.sh
```

Or if you need an interactive shell:
```
./docker/docker_run_interactive.sh
```

## Use
You should then be able to navigate to the following webpage on the machine you are running the Docker image on:
```
https://localhost:5080/
```
