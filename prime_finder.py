import numpy as np
import sys
import time
import json


def list_primes(limit):
    # Modified version of function in https://stackoverflow.com/a/11620052
    prime_list = [2]

    for num in range(3,limit,2):
        if all(num%i!=0 for i in range(3,int(np.sqrt(num))+1, 2)):
            prime_list.append(num)

    return prime_list

if __name__ == "__main__":
    # Get the limit from the command line
    limit = int(sys.argv[1])
    # Start the timer
    start = time.time()
    # List the primes
    prime_list = list_primes(limit)
    # Stop the timer
    end = time.time()
    # Calculate elapsed time
    dt = end-start
    # Make a dictionary
    output_dict = {
            "Primes":prime_list,
            "Runtime":dt
    }
    # Output as a JSON
    # From https://stackoverflow.com/a/26057360

    #Ben - Modifications from here downwards
    #Ben - disable write to file as no longer needed
    #with open('primes.json', 'w') as fp:
    #        json.dump(output_dict, fp)
    #Ben - print JSON instead so that output can be directly captured
    print(json.dumps(output_dict))
