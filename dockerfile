#Base image with Python3
FROM python:3

#Create the app directory
WORKDIR /usr/src/app

#transfer files from build directory
COPY . .

#Install necessary dependencies
RUN apt-get -y update
RUN pip3 install -r requirements.txt

#Open needed port
EXPOSE 5080

#Start the flask server
CMD ["python3", "./app.py"]
