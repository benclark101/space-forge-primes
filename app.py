####################################################################################################
#____ ___  ____ ____ ____    ____ ____ ____ ____ ____ 
#[__  |__] |__| |    |___    |___ |  | |__/ | __ |___ 
#___] |    |  | |___ |___    |    |__| |  \ |__] |___ 
#                                                     
#Prime Finder Flask App v1
#Author:benjmclark@gmail.com
#
#### Import necessary modules ######################################################################
from flask import Flask, render_template, request, redirect,jsonify, escape,url_for #for the web app
from flask_basicauth import BasicAuth #for app login
import subprocess #for calling the prime script
from datetime import datetime #for tracking the latency
import sys #for debug info to python console
import os #for working directory information

#--- SSL WSGI server setup (for production server)--------------------------------------------------
from cheroot.wsgi import Server as WSGIServer
from cheroot.wsgi import PathInfoDispatcher as WSGIPathInfoDispatcher
from cheroot.ssl.builtin import BuiltinSSLAdapter

#### User Variables ################################################################################
debugging = True #Variable to switch debugging on to find cause of errors

#### Main App Setup ################################################################################
app = Flask(__name__) #Create the Flask app

#Security - Basic User Authentication
app.config['BASIC_AUTH_USERNAME'] = 'TEST' #replaced for security - enter a username
app.config['BASIC_AUTH_PASSWORD'] = 'TEST' #replaced for security - enter a password
basic_auth = BasicAuth(app)

tld = os.getcwd()+"/" #get the top-level directory

#--- Index page --------------------------------------------
@app.route('/')
@basic_auth.required #Users need to be logged in to access this page
def spageforgehome():
	err = request.args.get('err') #Check the URL for any error prompts
	return render_template("index.html",err=err) #Render the homepage

#--- Page to display generated JSON output -----------------
@app.route('/primes', methods=['POST'])
@basic_auth.required #Users need to be logged in to access this page
def get_prime():
	submitted = request.form['action'] == 'Submit' #Check for submitted form using POST info
	prime = escape(request.form['prime']) #Sanitize input from 'prime' field for security
	if debugging == True:
		#Print information to console if debugging is on
		print("Prime:"+str(prime), file=sys.stderr)
	if submitted:
		#If the form has been submitted, then allow access to the page
		try:
			int(prime) #check that the santised input is acutally an integer (provided function does not accept float values)
		except ValueError:
			return redirect(url_for('spageforgehome', err="nan", code=307)) #send users back to the input form if not
		else:
			if int(prime) > 5000000:
				return redirect(url_for('spageforgehome', err="high", code=307)) #send users back to the input form if integer too high
			run_cmd = "python3 "+tld+"prime_finder.py "+str(prime) #generate the command which will launch the script
			if debugging == True: print(run_cmd, file=sys.stderr) #print the command if debugging is on
			json_return = subprocess.check_output(run_cmd,shell=True) #run the command on the server
			return render_template("prime.html",prime=str(int(prime)),json_return=json_return.decode().rstrip()) #render the output page and pass it the json output

#### Main App ######################################################################################
if __name__ == '__main__':
	#Run the app only when this module is the main program 
	#Setup the CherryPy WSGI (Web Server Gateway Interface)
	spaceforgeprime = WSGIPathInfoDispatcher({'/': app}) #load app from above
	server = WSGIServer(('0.0.0.0', 5080), spaceforgeprime) #launch app on port 5080, listen on all interfaces
	ssl_cert = tld+'ssl/fullchain.pem' #path to SSL certificate for encryption
	ssl_key = tld+'ssl/privkey.pem' #path to SSL key for encryption
	server.ssl_adapter =  BuiltinSSLAdapter(ssl_cert, ssl_key, None) #add SSL options to server
	try:
		server.start() #start server
	except KeyboardInterrupt:
		server.stop() #stop server on key interrupt
####################################################################################################		
